import os
import numba
import numpy
import matplotlib.pyplot as plt
import configparser
import time

start = time.time()


@numba.njit(parallel=True)
def magnetic_susceptibility(g, m, e_total, t, n):
    mag_sus = numpy.zeros(t.size)
    for i in numba.prange(t.size):
        mag_sus[i] = calc_ms(g, m, e_total, t[i], n)
    return mag_sus


@numba.njit
def calc_ms(g, m, e_total, t, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    p = stat / z
    m1 = (m * p).sum()
    m2 = (m ** 2 * p).sum()
    return (m2 - m1 ** 2) / (n * t)


@numba.njit(parallel=True)
def heat_capacity(g, e_total, t, n):
    c = numpy.zeros(t.size)
    for i in numba.prange(t.size):
        c[i] = calc_c(g, e_total, t[i], n)
    return c


@numba.njit
def calc_c(g, e_total, t, n):
    stat = g * numpy.exp(-e_total / t)
    z = stat.sum()
    p = stat / z
    e1 = (e_total * p).sum()
    e2 = (e_total ** 2 * p).sum()
    return (e2 - e1 ** 2) / (n * t ** 2)


conf = configparser.ConfigParser()
conf.read("data/conf.ini")
dos_dir = "data/dos" + conf["Sample"]["spins"] + "/"
stat_dir = "data/statsum" + conf["Sample"]["spins"] + "/"

T = numpy.logspace(int(conf["Temperature"]["start"]),
                   int(conf["Temperature"]["stop"]),
                   int(conf["Temperature"]["steps"]))
H = numpy.linspace(float(conf["Field"]["start"]),
                   float(conf["Field"]["stop"]),
                   int(conf["Field"]["steps"]))
H_com = numpy.linspace(int(conf["Field_for_comparison"]["start"]),
                       int(conf["Field_for_comparison"]["stop"]),
                       int(conf["Field_for_comparison"]["steps"]))

if conf["Recalculate"]["recalculate"] == "True":
    for h in H:
        P_plus = numpy.array([])
        c_max = numpy.array([])
        tc_max = numpy.array([])
        ms_max = numpy.array([])
        tms_max = numpy.array([])
        for file in os.listdir(dos_dir):
            file = dos_dir + file
            gem = numpy.loadtxt(open(file), skiprows=4).T
            N = int(open(file).readlines()[0].rstrip())
            J_sum = int(open(file).readlines()[2].rstrip())
            P_plus = numpy.append(P_plus, (2 * N * (N - 1) + J_sum) / (4 * N * (N - 1)))
            G = gem[0]
            E = gem[1]
            M = gem[2]
            E_total = E - M * h
            E_total += abs(numpy.min(E_total))
            hc = heat_capacity(G, E_total, T, N)
            ms = magnetic_susceptibility(G, M, E_total, T, N)
            c_max = numpy.append(c_max, numpy.max(hc))
            tc_max = numpy.append(tc_max, T[numpy.argmax(hc)])
            ms_max = numpy.append(ms_max, numpy.max(ms))
            tms_max = numpy.append(tms_max, numpy.argmax(ms))
            idx = P_plus.argsort()
            P_plus = P_plus[idx]
            c_max = c_max[idx]
            tc_max = tc_max[idx]
            ms_max = ms_max[idx]
            tms_max = tms_max[idx]
        numpy.save(stat_dir + "P_+_h" + str(h), P_plus)
        numpy.save(stat_dir + "c_max_h" + str(h), c_max)
        numpy.save(stat_dir + "t_max_h" + str(h), tc_max)
        numpy.savetxt(stat_dir + "t_max_h" + str(h) + ".txt", tc_max)
        numpy.save(stat_dir + "ms_max_h" + str(h), ms_max)
        numpy.save(stat_dir + "tms_max_h" + str(h), tms_max)

if conf["Recalculate"]["recalculate_comparison"] == "True":
    for h_com in H_com:
        c_max_com = numpy.array([])
        tc_max_com = numpy.array([])
        ms_max_com = numpy.array([])
        tms_max_com = numpy.array([])
        for file in os.listdir(dos_dir):
            file = dos_dir + file
            gem = numpy.loadtxt(open(file), skiprows=4).T
            N = int(open(file).readlines()[0].rstrip())
            G = gem[0]
            E = gem[1]
            M = gem[2]
            E_total = E - M * h_com
            E_total += abs(numpy.min(E_total))
            hc = heat_capacity(G, E_total, T, N)
            ms = magnetic_susceptibility(G, M, E_total, T, N)
            c_max_com = numpy.append(c_max_com, numpy.max(hc))
            tc_max_com = numpy.append(tc_max_com, numpy.argmax(hc))
            ms_max_com = numpy.append(ms_max_com, numpy.max(ms))
            tms_max_com = numpy.append(tms_max_com, numpy.argmax(ms))
        numpy.save(stat_dir + "comparison_c_max_h" + str(h_com), c_max_com)
        numpy.save(stat_dir + "comparison_t_max_h" + str(h_com), tc_max_com)
        numpy.save(stat_dir + "comparison_ms_max_h" + str(h_com), ms_max_com)
        numpy.save(stat_dir + "comparison_tms_max_h" + str(h_com), tms_max_com)

label = []
marks = ['8', '^', 'x', '+', '*', 'h', 'D', 'P', 'x', 'o']
# plt.figure(dpi=300)
for h in range(len(H)):
    P_plus_loaded = numpy.array([])
    c_max_loaded = numpy.array([])
    tc_max_loaded = numpy.array([])
    ms_max_loaded = numpy.array([])
    tms_max_loaded = numpy.array([])
    idx = P_plus_loaded.argsort()
    P_plus_loaded = P_plus_loaded[idx]
    c_max_loaded = c_max_loaded[idx]
    tc_max_loaded = tc_max_loaded[idx]
    ms_max_loaded = ms_max_loaded[idx]
    tms_max_loaded = tms_max_loaded[idx]
    for file in os.listdir(dos_dir):
        P_plus_loaded = numpy.load(stat_dir + "P_+_h" + str(H[h]) + ".npy")
        c_max_loaded = numpy.load(stat_dir + "c_max_h" + str(H[h]) + ".npy")
        tc_max_loaded = numpy.load(stat_dir + "t_max_h" + str(H[h]) + ".npy")
        ms_max_loaded = numpy.load(stat_dir + "ms_max_h" + str(H[h]) + ".npy")
        tms_max_loaded = numpy.load(stat_dir + "tms_max_h" + str(H[h]) + ".npy")
    if conf["Plot"]["plot"] == "hc_max":
        plt.scatter(P_plus_loaded, c_max_loaded, s=5)
        plt.xlabel("$P_{+}$")
        plt.ylabel("$C_{max}$")
        label.append(f"H = {H[h]}")
        plt.legend(label)
    elif conf["Plot"]["plot"] == "T_hc_max":
        plt.scatter(P_plus_loaded, tc_max_loaded, s=5)
        plt.xlabel("$P_{+}$")
        plt.ylabel("$T_{C_{max}}$")
        label.append(f"H = {H[h]}")
        plt.legend(label)
    elif conf["Plot"]["plot"] == "ms_max":
        plt.scatter(P_plus_loaded, ms_max_loaded, s=5)
        plt.xlabel("$P_{+}$")
        plt.ylabel("$ms_{max}$")
        label.append(f"H = {H[h]}")
        plt.legend(label)
    elif conf["Plot"]["plot"] == "T_ms_max":
        plt.scatter(P_plus_loaded, tms_max_loaded, s=5)
        plt.xlabel("$P_{+}$")
        plt.ylabel("$T_{ms_{max}}$")
        label.append(f"H = {H[h]}")
        plt.legend(label)
    elif conf["Plot"]["plot"] == "T_aver_max":
        left = int(P_plus_loaded.size * 0.5 - 5)
        right = int(P_plus_loaded.size * 0.5 + 5)
        T = numpy.array(tc_max_loaded[left:right])
        T_aver = T.sum() / T.size
        plt.scatter(T_aver, H[h], s=8, color='black')
        plt.xlabel("$T_{C_{max}}$", fontsize=18)
        plt.tick_params(axis='both', which='major', labelsize=12)
        plt.ylabel("$H$", fontsize=18)
        # plt.xlim([1.3, 1.5])

if conf["Plot"]["plot"] == "hc_max_com":
    c_h = numpy.array([])
    for h in range(len(H_com)):
        c_max_com_loaded = numpy.array([])
        tc_max_com_loaded = numpy.array([])
        ms_max_com_loaded = numpy.array([])
        tms_max_com_loaded = numpy.array([])
        for file in os.listdir(dos_dir):
            c_max_com_loaded = numpy.load(stat_dir + "comparison_c_max_h" + str(H_com[h]) + ".npy")
            tc_max_com_loaded = numpy.load(stat_dir + "comparison_t_max_h" + str(H_com[h]) + ".npy")
            ms_max_com_loaded = numpy.load(stat_dir + "comparison_ms_max_h" + str(H_com[h]) + ".npy")
            tms_max_com_loaded = numpy.load(stat_dir + "comparison_tms_max_h" + str(H_com[h]) + ".npy")
        c_h = numpy.append(c_h, c_max_com_loaded[int(conf["Plot"]["sample_plot_com"])])
    print(numpy.shape(c_h))
    print(numpy.shape(H_com))
    if conf["Plot"]["plot"] == "hc_max_com":
        plt.scatter(H_com, c_h, s=5)
        plt.xlabel("$H$")
        plt.ylabel("$C_{max}$")
end = time.time() - start
print(end)
plt.savefig("data/results/hc_ms.eps")
plt.show()
